<!DOCTYPE html>
<html lang="en">
<head>
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="depan">
        <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="belakang">
        <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singpore">Singapore</option>
            <option value="thailand">Thailand</option>
        </select>
        <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" name="sign up" value="Sign Up">
    </form>
</body>
</html>